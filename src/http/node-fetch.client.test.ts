import 'jest';
import fetch, { RequestInfo, RequestInit, FetchError } from 'node-fetch';
import { NodeFetchClient } from './node-fetch.client';

const { Response } = jest.requireActual('node-fetch');
jest.mock('node-fetch');

const CONFIG = {
    url: 'https://foo',
    clientId: 'my-app:default',
    clientSecret: 'SEKRIT',
};

describe('Node Fetch Client should...', () => {
    let client: NodeFetchClient;

    beforeEach(() => {
        // Mock response from node-fetch
        (fetch as jest.MockedFunction<typeof fetch>).mockImplementation(
            async (url: RequestInfo, init?: RequestInit) => {
                let status = 200;
                let statusText = 'OK';

                if (init.method === 'TEST-500') {
                    status = 500;
                    statusText = 'Something went wrong (HTTP)';
                }

                if (init.method === 'TEST-FETCHERROR') {
                    throw new FetchError('Something went wrong (FETCH)', 'TEST');
                }

                const response = new Response(
                    JSON.stringify({
                        test: 'TEST',
                    }),
                    {
                        status,
                        statusText,
                    },
                );

                return response;
            },
        );

        client = new NodeFetchClient(CONFIG);
    });

    test('exist', () => {
        expect(NodeFetchClient).toBeDefined();
    });

    test('send params as key-value pairs for form-urlencoded contentType', async () => {
        await client.send({
            method: 'TEST',
            path: '/',
            contentType: 'application/x-www-form-urlencoded',
            data: {
                a: 'b',
                x: 'y',
            },
        });

        expect(fetch).toHaveBeenCalledWith(
            'https://foo/',
            expect.objectContaining({
                body: 'a=b&x=y',
            }),
        );
    });

    test('send Bearer header when token provided', async () => {
        client = new NodeFetchClient(Object.assign({ flatten: true }, CONFIG));

        await client.send({
            method: 'TEST',
            path: '/',
            token: 'abc',
        });

        expect(fetch).toHaveBeenCalledWith(
            'https://foo/',
            expect.objectContaining({
                headers: expect.objectContaining({ Authorization: 'Bearer abc' }),
            }),
        );
    });

    test('send X-Languages header when configured', async () => {
        client = new NodeFetchClient(Object.assign({ languages: ['en', 'es'] }, CONFIG));

        await client.send({
            method: 'TEST',
            path: '/',
            token: 'abc',
        });

        expect(fetch).toHaveBeenCalledWith(
            'https://foo/',
            expect.objectContaining({
                headers: expect.objectContaining({ 'X-Languages': 'en,es' }),
            }),
        );
    });

    test('send X-Flatten header when configured', async () => {
        client = new NodeFetchClient(Object.assign({ flatten: true }, CONFIG));

        await client.send({
            method: 'TEST',
            path: '/',
            token: 'abc',
        });

        expect(fetch).toHaveBeenCalledWith(
            'https://foo/',
            expect.objectContaining({
                headers: expect.objectContaining({ 'X-Flatten': 'true' }),
            }),
        );
    });

    test('send X-Unpublished header when configured', async () => {
        await client.send({
            method: 'TEST',
            path: '/',
            token: 'abc',
            draft: true,
        });

        expect(fetch).toHaveBeenCalledWith(
            'https://foo/',
            expect.objectContaining({
                headers: expect.objectContaining({ 'X-Unpublished': 'true' }),
            }),
        );
    });

    test('handles server errors', async () => {
        expect.assertions(1);

        try {
            await client.send({
                method: 'TEST-500',
                path: '/',
            });
        } catch (e) {
            expect(e.message).toMatch(/HTTP error/);
        }
    });

    test('handles fetch errors', async () => {
        expect.assertions(1);

        try {
            await client.send({
                method: 'TEST-FETCHERROR',
                path: '/',
            });
        } catch (e) {
            expect(e.message).toMatch(/Fetch error/);
        }
    });
});
