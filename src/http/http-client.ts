export class HttpSendParams {
    /** HTTP method */
    method: string;
    /** Will be appended to SquidexClientConfiguration.url */
    path: string;
    /** Data to send */
    data?: any;
    /** HTTP content type */
    contentType?: string;
    /** Bearer token to send with request */
    token?: string;
    /** Include drafts */
    draft?: boolean;
}

/**
 * Base class for HTTP clients
 */
export abstract class HttpClient {
    /**
     * Sends a HTTP request
     */
    public abstract async send<TReq = any, TRes = TReq>(params: HttpSendParams): Promise<TRes>;
}
