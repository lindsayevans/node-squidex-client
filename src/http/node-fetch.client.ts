import fetch, { FetchError } from 'node-fetch';

import { SquidexClientConfiguration } from '../configuration';
import { HttpClient, HttpSendParams } from './http-client';

/**
 * Basically just wrapper around node-fetch, with some Squidex specific things & error handling
 */
export class NodeFetchClient implements HttpClient {
    constructor(private readonly config: SquidexClientConfiguration) {}

    public async send<TReq = any, TRes = TReq>(params: HttpSendParams): Promise<TRes> {
        if (params.contentType === 'application/x-www-form-urlencoded') {
            params.data = new URLSearchParams(params.data).toString();
        } else {
            params.data = JSON.stringify(params.data);
        }

        const headers: { [key: string]: string } = {};

        if (params.contentType === undefined) {
            params.contentType = 'application/json';
        }
        headers['Content-Type'] = params.contentType;

        if (params.token !== undefined) {
            headers['Authorization'] = `Bearer ${params.token}`;

            if (this.config.languages !== undefined) {
                headers['X-Languages'] = this.config.languages.join(',');
            }

            if (this.config.flatten) {
                headers['X-Flatten'] = 'true';
            }

            if (params.draft) {
                headers['X-Unpublished'] = 'true';
            }
        }

        const request = {
            method: params.method,
            body: params.data,
            headers,
        };

        try {
            const response = await fetch(this.config.url + params.path, request);

            // Catch error responses & throw an error
            if (!response.ok) {
                const body = await response.text();
                throw new Error(`[HttpClient] HTTP error ${response.status}: ${response.statusText} - ${body}`);
            }

            if (response.status === 204) {
                return;
            } else {
                const responseData = await response.json();
                return responseData as TRes;
            }
        } catch (e) {
            if (e instanceof FetchError) {
                throw new Error(`[HttpClient] Fetch error (${e.type}): ${e.message}`);
            }

            throw e;
        }
    }
}
