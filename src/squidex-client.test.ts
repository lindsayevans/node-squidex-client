import 'jest';
import { SquidexClient } from './squidex-client';
import { HttpClient } from './http';
import { NodeFetchClient } from './http/node-fetch.client';

const CONFIG = {
    clientId: 'my-app:default',
    clientSecret: 'SEKRIT',
};

describe('Squidex Client should...', () => {
    test('exist', () => {
        expect(SquidexClient).toBeDefined();
    });

    test('be configured', () => {
        const client = new SquidexClient(CONFIG);

        expect(client.config).toBeDefined();
    });

    test('use provided HttpClient', () => {
        const mockHttpClient = ({
            isMock: true,
        } as unknown) as HttpClient;

        const client = new SquidexClient(CONFIG, mockHttpClient);

        expect((client as any).httpClient).toMatchObject({ isMock: true });
    });

    test('use NodeFetchClient if none provided', () => {
        const client = new SquidexClient(CONFIG);

        expect((client as any).httpClient).toBeInstanceOf(NodeFetchClient);
    });

    test('set schema properly when calling content', () => {
        const client = new SquidexClient(CONFIG);
        const contentSvc = client.content('test');

        expect(contentSvc.schema).toBe('test');
    });

    test('throw error when calling content without schema', () => {
        expect.assertions(1);
        try {
            const client = new SquidexClient(CONFIG);
            // Need to cast to `any` so that TS lets us do this
            (client as any).content();
        } catch (e) {
            expect(e.message).toMatch(/Schema is required/);
        }
    });
});
