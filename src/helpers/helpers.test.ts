import 'jest';
import { Helpers } from './helpers';
import { SquidexClientConfiguration } from '../configuration';

const CONFIG = {
    url: 'test://test',
    appName: 'test-app',
    clientId: 'my-app:default',
    clientSecret: 'SEKRIT',
} as SquidexClientConfiguration;

describe('Helpers should...', () => {
    let helpers: Helpers;

    beforeEach(() => {
        helpers = new Helpers(CONFIG);
    });

    test('exist', () => {
        expect(Helpers).toBeDefined();
    });

    test('return correct asset URL for a given ID', () => {
        const url = helpers.getAssetUrl('TEST');

        expect(url).toBe('test://test/api/assets/test-app/TEST?download=0');
    });

    test('include download flag if set', () => {
        const url = helpers.getAssetUrl('TEST', true);

        expect(url).toBe('test://test/api/assets/test-app/TEST?download=1');
    });
});
