import { SquidexClientConfiguration } from '../configuration';

export class Helpers {
    constructor(private readonly config: SquidexClientConfiguration) {}

    getAssetUrl(id: string, download = false): string {
        return `${this.config.url}/api/assets/${this.config.appName}/${id}?download=${download ? 1 : 0}`;
    }
}
