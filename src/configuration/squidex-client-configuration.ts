/**
 * Configuration options for the Squidex Client
 */
export class SquidexClientConfiguration {
    /** Base URL for the Squidex API */
    url? = 'https://cloud.squidex.io';
    /** Application name - will be inferred from `clientId` id not defined */
    appName?: string;
    /** Client ID */
    clientId: string;
    /** Client secret */
    clientSecret: string;
    /** Languages to return */
    languages?: string[];
    /** Flatten invariants */
    flatten?: boolean = false;
    foo?: boolean;
}
