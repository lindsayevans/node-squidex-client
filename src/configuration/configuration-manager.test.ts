import 'jest';
import { ConfigurationManager } from './configuration-manager';

describe('Configuration manager should...', () => {
    test('exist', () => {
        expect(ConfigurationManager).toBeDefined();
    });

    test('use default URL', () => {
        const config = ConfigurationManager.buildConfiguration({
            clientId: 'my-app:default',
            clientSecret: 'SEKRIT',
        });

        expect(config.url).toBeDefined();
        expect(config.url).toBe('https://cloud.squidex.io');
    });

    test('use supplied URL', () => {
        const config = ConfigurationManager.buildConfiguration({
            url: 'https://my-app',
            clientId: 'my-app:default',
            clientSecret: 'SEKRIT',
        });

        expect(config.url).toBeDefined();
        expect(config.url).toBe('https://my-app');
    });

    test('infer appName if missing', () => {
        const config = ConfigurationManager.buildConfiguration({
            clientId: 'my-app:default',
            clientSecret: 'SEKRIT',
        });

        expect(config.appName).toBe('my-app');
    });

    test('throw error for missing config', () => {
        expect.assertions(1);
        try {
            // Need to cast to `any` so that TS lets us do this
            (ConfigurationManager as any).buildConfiguration();
        } catch (e) {
            expect(e.message).toMatch(/Configuration/);
        }
    });

    test('throw error for missing clientId', () => {
        expect.assertions(1);
        try {
            // Need to cast to `any` so that TS lets us do this
            (ConfigurationManager as any).buildConfiguration({});
        } catch (e) {
            expect(e.message).toMatch(/clientId/);
        }
    });

    test('throw error for malformed clientId', () => {
        expect.assertions(1);
        try {
            // Need to cast to `any` so that TS lets us do this
            (ConfigurationManager as any).buildConfiguration({
                clientId: 'my-appdefault',
                clientSecret: 'SEKRIT',
            });
        } catch (e) {
            expect(e.message).toMatch(/clientId/);
        }
    });

    test('throw error for missing clientSecret', () => {
        expect.assertions(1);
        try {
            // Need to cast to `any` so that TS lets us do this
            (ConfigurationManager as any).buildConfiguration({
                clientId: 'my-app:default',
            });
        } catch (e) {
            expect(e.message).toMatch(/clientSecret/);
        }
    });
});
