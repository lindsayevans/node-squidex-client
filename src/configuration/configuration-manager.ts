import { SquidexClientConfiguration } from './squidex-client-configuration';

export class ConfigurationManager {
    public static buildConfiguration(
        options: Partial<SquidexClientConfiguration>,
        ...extraOptions: Partial<SquidexClientConfiguration>[]
    ): SquidexClientConfiguration {
        // Validate configuration options

        if (options === undefined) {
            throw new Error('[SquidexClient] Configuration options are required, at least `clientId` & `clientSecret`');
        }

        if (options.clientId === undefined) {
            throw new Error('[SquidexClient] Configuration option `clientId` is required');
        }

        if (options.clientId.split(':').length < 2) {
            throw new Error(
                '[SquidexClient] Configuration option `clientId` must be in the format `APP_NAME:CLIENT_NAME`',
            );
        }

        if (options.clientSecret === undefined) {
            throw new Error('[SquidexClient] Configuration option `clientSecret` is required');
        }

        // Infer the application name from the Client ID if not supplied
        if (options.appName === undefined) {
            options.appName = options.clientId.split(':')[0];
        }

        // Merge configuration
        return Object.assign({}, new SquidexClientConfiguration(), options, ...extraOptions);
    }
}
