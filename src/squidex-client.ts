import { SquidexClientConfiguration, ConfigurationManager } from './configuration';
import { HttpClient } from './http';
import { NodeFetchClient } from './http/node-fetch.client';
import { AuthenticationManager } from './authentication/authentication-manager';
import { ContentSchema } from './schemas/content';
import { Helpers } from './helpers';

export class SquidexClient {
    /** Configuration options in use by the client */
    public config: SquidexClientConfiguration;

    /** Helper methods */
    public helpers: Helpers;

    private authenticationManager: AuthenticationManager;
    private contentSchema: ContentSchema;

    /**
     * NodeJS client for the Squidex CMS
     *
     * @example
     * const client = new SquidexClient({
     *  appName: 'my-app',
     *  clientId: 'my-app:default',
     *  clientSecret: 'SEKRIT'
     * })
     * const posts = await client.query<BlogPostDTO>('Posts')
     */
    constructor(options: Partial<SquidexClientConfiguration>, private httpClient?: HttpClient) {
        this.config = ConfigurationManager.buildConfiguration(options);

        if (this.httpClient === undefined) {
            this.httpClient = new NodeFetchClient(this.config);
        }

        this.authenticationManager = new AuthenticationManager(this.config, this.httpClient);

        this.contentSchema = new ContentSchema(this.config, this.httpClient, this.authenticationManager);

        this.helpers = new Helpers(this.config);
    }

    /** Exposes methods for querying Squidex content for a particular schema */
    public content(schema: string): ContentSchema {
        if (schema === undefined) {
            throw new Error('[SquidexClient] Schema is required');
        }

        this.contentSchema.schema = schema;

        return this.contentSchema;
    }

    // TODO:
    // * Expose query methods for other admin type API endpoints - https://cloud.squidex.io/api/docs
}
