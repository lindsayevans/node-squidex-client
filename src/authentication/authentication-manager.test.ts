import 'jest';
import { AuthenticationManager } from './authentication-manager';
import { HttpClient } from '../http/http-client';
jest.mock('../http/http-client');

const TOKEN = 'abc-123';

describe('Authentication Manager should...', () => {
    let httpClient: HttpClient;
    let authenticationManager: AuthenticationManager;

    beforeEach(() => {
        // Mock the identity server response
        HttpClient.prototype.send = jest.fn().mockResolvedValue({
            /* eslint-disable @typescript-eslint/camelcase */
            access_token: TOKEN,
            expires_in: 123456,
            token_type: '?',
            scope: '?',
            /* eslint-enable */
        });
        httpClient = new (HttpClient as any)();

        authenticationManager = new AuthenticationManager(
            {
                url: 'https://foo',
                clientId: 'my-app:default',
                clientSecret: 'SEKRIT',
            },
            httpClient,
        );
    });

    test('exist', () => {
        expect(AuthenticationManager).toBeDefined();
    });

    test('call identity server', async () => {
        await authenticationManager.authenticate();

        expect(httpClient.send).toHaveBeenCalledTimes(1);
        expect(httpClient.send).toHaveBeenCalledWith(
            expect.objectContaining({
                data: {
                    /* eslint-disable @typescript-eslint/camelcase */
                    client_id: 'my-app:default',
                    client_secret: 'SEKRIT',
                    grant_type: 'client_credentials',
                    scope: 'squidex-api',
                    /* eslint-enable */
                },
                path: '/identity-server/connect/token',
            }),
        );
    });

    test('not call identity server after authentication', async () => {
        await authenticationManager.authenticate();
        await authenticationManager.authenticate();

        expect(httpClient.send).toHaveBeenCalledTimes(1);
    });

    test('call identity server after expiry', async () => {
        await authenticationManager.authenticate();

        // Reset expiry date to 2 days ago
        const now = new Date();
        now.setDate(now.getDate() - 2);
        (authenticationManager as any).accessTokenExpiresOn = new Date(now.toUTCString());

        await authenticationManager.authenticate();

        expect(httpClient.send).toHaveBeenCalledTimes(2);
    });

    test('resolve with token', async () => {
        const token = await authenticationManager.authenticate();
        expect(token).toBe(TOKEN);
    });
});
