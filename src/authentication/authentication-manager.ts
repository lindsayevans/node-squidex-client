import { SquidexClientConfiguration } from '../configuration';
import { SquidexOAuthRequest } from './squidex-oauth-request';
import { SquidexOAuthResponse } from './squidex-oauth-response';
import { HttpClient } from '../http';

/**
 * Handles authentication for Squidex requests
 */
export class AuthenticationManager {
    private accessToken: string;
    private accessTokenExpiresOn: Date;

    constructor(private readonly config: SquidexClientConfiguration, private readonly httpClient: HttpClient) {}

    /**
     * Authenticate with the Squidex server
     *
     * Resolves with the access token
     */
    public async authenticate(): Promise<string> {
        if (this.requiresAuthentication()) {
            delete this.accessToken;
            delete this.accessTokenExpiresOn;

            const response = await this.httpClient.send<SquidexOAuthRequest, SquidexOAuthResponse>({
                method: 'POST',
                path: '/identity-server/connect/token',
                data: {
                    /* eslint-disable @typescript-eslint/camelcase */
                    grant_type: 'client_credentials',
                    scope: 'squidex-api',
                    client_secret: this.config.clientSecret,
                    client_id: this.config.clientId,
                    /* eslint-enable  */
                },
                contentType: 'application/x-www-form-urlencoded',
            });

            /* eslint-disable @typescript-eslint/camelcase */
            this.accessToken = response.access_token;
            /* eslint-enable */

            const now = new Date();
            /* eslint-disable @typescript-eslint/camelcase */
            now.setTime(now.getTime() + response.expires_in * 1000);
            /* eslint-enable */
            this.accessTokenExpiresOn = new Date(now.toUTCString());
        }

        return this.accessToken;
    }

    private requiresAuthentication(): boolean {
        if (this.accessToken === undefined || this.accessTokenExpiresOn === undefined) {
            return true;
        }

        // Check 1 day before token expiry, just to be sure
        const now = new Date();
        now.setDate(now.getDate() - 1);

        return this.accessTokenExpiresOn < new Date(now.toUTCString());
    }
}
