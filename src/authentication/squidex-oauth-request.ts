export class SquidexOAuthRequest {
    grant_type: string;
    scope: string;
    client_secret: string;
    client_id: string;
}
