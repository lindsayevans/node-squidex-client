/** A collection of Squidex content */
export class SquidexContentCollection<T> {
    /** The total number of items */
    total: number;
    /** The content items */
    items: T[];
}
