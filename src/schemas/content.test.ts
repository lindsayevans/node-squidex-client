import 'jest';
import { ContentSchema } from './content';
import { HttpClient } from '../http/http-client';
import { AuthenticationManager } from '../authentication/authentication-manager';
import { SquidexJsonQuery } from './squidex-query';
jest.mock('../http/http-client');

const TOKEN = 'abc-123';
const CONFIG = {
    url: 'https://foo',
    appName: 'my-app',
    clientId: 'my-app:default',
    clientSecret: 'SEKRIT',
};

describe('Content Schema should...', () => {
    let httpClient: HttpClient;
    let authenticationManager: AuthenticationManager;
    let contentSchema: ContentSchema;

    beforeEach(() => {
        // Mock the API response
        HttpClient.prototype.send = jest.fn().mockResolvedValue({
            items: [
                {
                    data: { test: 'TEST1' },
                    created: '1970-01-01T00:00:00.000Z',
                    lastModified: '1978-01-24T03:17:00.000Z',
                },
                {
                    data: { test: 'TEST2' },
                    created: '1970-01-01T00:00:00.000Z',
                    lastModified: '1978-01-24T03:17:00.000Z',
                },
                {
                    data: { test: 'TEST3' },
                    created: '1970-01-01T00:00:00.000Z',
                    lastModified: '1978-01-24T03:17:00.000Z',
                },
            ],
        });

        httpClient = new (HttpClient as any)();

        AuthenticationManager.prototype.authenticate = jest.fn().mockResolvedValue(TOKEN);
        authenticationManager = new (AuthenticationManager as any)(CONFIG, httpClient);

        contentSchema = new ContentSchema(CONFIG, httpClient, authenticationManager);
        contentSchema.schema = 'test';
    });

    test('exist', () => {
        expect(AuthenticationManager).toBeDefined();
    });

    test('call the API server', async () => {
        await contentSchema.query();

        expect(httpClient.send).toHaveBeenCalledTimes(1);
        expect(httpClient.send).toHaveBeenCalledWith(
            expect.objectContaining({
                path: '/api/content/my-app/test/?',
            }),
        );
    });

    test('send the token', async () => {
        await contentSchema.query();

        expect(httpClient.send).toHaveBeenCalledTimes(1);
        expect(httpClient.send).toHaveBeenCalledWith(
            expect.objectContaining({
                token: 'abc-123',
            }),
        );
    });

    test('include the serialised query in the URL', async () => {
        await contentSchema.query({ $top: 10 });

        expect(httpClient.send).toHaveBeenCalledTimes(1);
        expect(httpClient.send).toHaveBeenCalledWith(
            expect.objectContaining({
                path: '/api/content/my-app/test/?%24top=10',
            }),
        );
    });

    test('include drafts', async () => {
        await contentSchema.query({}, true);

        expect(httpClient.send).toHaveBeenCalledTimes(1);
        expect(httpClient.send).toHaveBeenCalledWith(
            expect.objectContaining({
                draft: true,
            }),
        );
    });

    test('deserialise dates correctly', async () => {
        const result = await contentSchema.query();

        expect(result.items[0].created).toBeInstanceOf(Date);
        expect(result.items[0].created.getFullYear()).toBe(1970);
        expect(result.items[0].lastModified).toBeInstanceOf(Date);
        expect(result.items[0].lastModified.getFullYear()).toBe(1978);
    });

    test('not overwrite JSON query for querySingle()', async () => {
        const query = new SquidexJsonQuery();
        query.q = { skip: 2 };
        await contentSchema.querySingle(query);

        expect(httpClient.send).toHaveBeenCalledTimes(1);
        expect(httpClient.send).toHaveBeenCalledWith(
            expect.objectContaining({
                path: '/api/content/my-app/test/?q=%7B%22skip%22%3A2%7D',
            }),
        );
    });

    test('only request the first result for querySingle()', async () => {
        await contentSchema.querySingle();

        expect(httpClient.send).toHaveBeenCalledTimes(1);
        expect(httpClient.send).toHaveBeenCalledWith(
            expect.objectContaining({
                path: '/api/content/my-app/test/?%24top=1',
            }),
        );
    });

    test('return all data for query()', async () => {
        const result = await contentSchema.query();

        expect(result.items.length).toBe(3);
    });

    test('return first result for querySingle()', async () => {
        const result = await contentSchema.querySingle();

        expect(result).toMatchObject({ data: { test: 'TEST1' } });
    });

    test('send a POST request with the content for create()', async () => {
        await contentSchema.create({ test: 'TEST' });

        expect(httpClient.send).toHaveBeenCalledTimes(1);
        expect(httpClient.send).toHaveBeenCalledWith(
            expect.objectContaining({
                path: '/api/content/my-app/test/?publish=false',
                method: 'POST',
                data: { test: 'TEST' },
            }),
        );
    });

    test('include the publish parameter for create({}, true)', async () => {
        await contentSchema.create({}, true);

        expect(httpClient.send).toHaveBeenCalledTimes(1);
        expect(httpClient.send).toHaveBeenCalledWith(
            expect.objectContaining({
                path: '/api/content/my-app/test/?publish=true',
            }),
        );
    });

    test('send a PUT request with the content for update()', async () => {
        await contentSchema.update('123-456', { test: 'TEST' });

        expect(httpClient.send).toHaveBeenCalledTimes(1);
        expect(httpClient.send).toHaveBeenCalledWith(
            expect.objectContaining({
                path: '/api/content/my-app/test/123-456',
                method: 'PUT',
                data: { test: 'TEST' },
            }),
        );
    });

    test('send a PATCH request with the content for patch()', async () => {
        await contentSchema.patch('123-456', { test: 'TEST' });

        expect(httpClient.send).toHaveBeenCalledTimes(1);
        expect(httpClient.send).toHaveBeenCalledWith(
            expect.objectContaining({
                path: '/api/content/my-app/test/123-456',
                method: 'PATCH',
                data: { test: 'TEST' },
            }),
        );
    });

    test('send a DELETE request for delete()', async () => {
        await contentSchema.delete('123-456');

        expect(httpClient.send).toHaveBeenCalledTimes(1);
        expect(httpClient.send).toHaveBeenCalledWith(
            expect.objectContaining({
                path: '/api/content/my-app/test/123-456',
                method: 'DELETE',
            }),
        );
    });

    test('send a PUT request for updateStatus()', async () => {
        await contentSchema.updateStatus('123-456', { status: 'Testing' });

        expect(httpClient.send).toHaveBeenCalledTimes(1);
        expect(httpClient.send).toHaveBeenCalledWith(
            expect.objectContaining({
                path: '/api/content/my-app/test/123-456/status',
                method: 'PUT',
            }),
        );
    });

    test('send DueTime for updateStatus() if provided', async () => {
        await contentSchema.updateStatus('123-456', { status: 'Testing', dueTime: new Date('2424-01-24T00:00Z') });

        expect(httpClient.send).toHaveBeenCalledTimes(1);
        expect(httpClient.send).toHaveBeenCalledWith(
            expect.objectContaining({
                path: '/api/content/my-app/test/123-456/status',
                method: 'PUT',
                data: { DueTime: '2424-01-24T00:00:00.000Z', Status: 'Testing' },
            }),
        );
    });

    test('send a PUT request for discard()', async () => {
        await contentSchema.discard('123-456');

        expect(httpClient.send).toHaveBeenCalledTimes(1);
        expect(httpClient.send).toHaveBeenCalledWith(
            expect.objectContaining({
                path: '/api/content/my-app/test/123-456/discard',
                method: 'PUT',
            }),
        );
    });
});
