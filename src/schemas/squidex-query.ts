export type SquidexQuery = SquidexODataQuery | SquidexJsonQuery;

export class SquidexODataQuery {
    $top?: number;
    $skip?: number;
    $search?: string;
    $orderby?: string;
    $filter?: string;
}

export class SquidexJsonQuery {
    q?: any;
}
