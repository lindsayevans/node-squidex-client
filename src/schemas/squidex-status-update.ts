export class SquidexStatusUpdate {
    status: string;
    dueTime?: Date;
}
