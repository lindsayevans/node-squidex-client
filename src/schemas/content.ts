import { SquidexClientConfiguration } from '../configuration';
import { AuthenticationManager } from '../authentication/authentication-manager';
import { HttpClient } from '../http';
import { SquidexQuery, SquidexJsonQuery } from './squidex-query';
import { SquidexContent } from './squidex-content';
import { SquidexContentCollection } from './squidex-content-collection';
import { SquidexStatusUpdate } from './squidex-status-update';

export class ContentSchema {
    public schema: string;

    constructor(
        private readonly config: SquidexClientConfiguration,
        private readonly httpClient: HttpClient,
        private readonly authenticationManager: AuthenticationManager,
    ) {}

    /**
     * Returns all content from the API matching `query`
     * Set `draft` to true to return content in draft
     * */
    public async query<T = any>(
        query: SquidexQuery = {},
        draft = false,
    ): Promise<SquidexContentCollection<SquidexContent<T>>> {
        const token = await this.authenticationManager.authenticate();
        const path = this.buildPath('', query);

        const response = await this.httpClient.send<T, SquidexContentCollection<SquidexContent<T>>>({
            method: 'GET',
            path,
            token,
            draft,
        });

        response.items = response.items.map(x => {
            return this.convertFromJson<T>(x);
        });

        return response;
    }

    /**
     * Returns the first content item from the API matching `query`
     * Set `draft` to true to return content in draft
     * */
    public async querySingle<T = any>(query: SquidexQuery = {}, draft = false): Promise<SquidexContent<T>> {
        if (!(query instanceof SquidexJsonQuery)) {
            query.$top = 1;
        }

        const response = await this.query(query, draft);

        return response.items[0];
    }

    /**
     * Creates a new content item
     *
     * @param publish whether to publish the content item immediately
     */
    public async create<T = any, R = T>(content: T, publish = false): Promise<SquidexContent<R>> {
        const token = await this.authenticationManager.authenticate();
        const path = this.buildPath('', { publish });

        const response = await this.httpClient.send<T, SquidexContent<R>>({
            method: 'POST',
            path,
            token,
            data: content,
        });

        return this.convertFromJson<R>(response);
    }

    /**
     * Updates a content item
     *
     * @param id ID of the content item
     */
    public async update<T = any>(id: string, content: T): Promise<SquidexContent<T>> {
        const token = await this.authenticationManager.authenticate();
        const path = this.buildPath(id);

        const response = await this.httpClient.send<T, SquidexContent<T>>({
            method: 'PUT',
            path,
            token,
            data: content,
        });

        return this.convertFromJson<T>(response);
    }

    /**
     * Patches a content item
     *
     * @param id ID of the content item
     */
    public async patch<T = any>(id: string, content: Partial<T>): Promise<SquidexContent<T>> {
        const token = await this.authenticationManager.authenticate();
        const path = this.buildPath(id);

        const response = await this.httpClient.send<T, SquidexContent<T>>({
            method: 'PATCH',
            path,
            token,
            data: content,
        });

        return this.convertFromJson<T>(response);
    }

    /**
     * Deletes a content item
     *
     * @param id ID of the content item
     */
    public async delete<T = any>(id: string): Promise<boolean> {
        const token = await this.authenticationManager.authenticate();
        const path = this.buildPath(id);

        await this.httpClient.send<T, boolean>({
            method: 'DELETE',
            path,
            token,
        });

        return true;
    }

    /**
     * Updates the status of a content item
     *
     * @param id ID of the content item
     * @param status the new status
     */
    public async updateStatus<T = any>(id: string, status: SquidexStatusUpdate): Promise<SquidexContent<T>> {
        const token = await this.authenticationManager.authenticate();
        const path = this.buildPath(id + '/status');

        const data: { [key: string]: string } = {
            Status: status.status,
        };

        if (status.dueTime !== undefined) {
            data.DueTime = status.dueTime.toISOString();
        }

        const response = await this.httpClient.send<T, SquidexContent<T>>({
            method: 'PUT',
            path,
            token,
            data,
        });

        return this.convertFromJson<T>(response);
    }

    /**
     * Discards changes to a content item
     *
     * @param id ID of the content item
     */
    public async discard<T = any>(id: string): Promise<SquidexContent<T>> {
        const token = await this.authenticationManager.authenticate();
        const path = this.buildPath(id + '/discard');

        const response = await this.httpClient.send<T, SquidexContent<T>>({
            method: 'PUT',
            path,
            token,
        });

        return this.convertFromJson<T>(response);
    }

    // Converts dates etc.
    private convertFromJson<T>(json: any): SquidexContent<T> {
        const res = Object.assign({}, json);
        res.created = new Date(json.created);
        res.lastModified = new Date(json.lastModified);
        return res;
    }

    // TODO: Extract to helper
    private buildPath(suffix = '', params?: any): string {
        let path = `/api/content/${this.config.appName}/${this.schema}/${suffix}`;

        if (params !== undefined && params !== {}) {
            if (params instanceof SquidexJsonQuery) {
                params.q = JSON.stringify(params.q);
            }
            const qs = new URLSearchParams(params);
            path += '?' + qs;
        }

        return path;
    }
}
