export class SquidexContent<T> {
    /** The id of the content */
    id: string;
    /** The data of the content */
    data: T;
    /** The draft data of the content */
    dataDraft: T;
    /** The version of the content */
    version: number;
    /** The date and time when the content has been created */
    created: Date;
    /** The user that has created the content */
    createdBy: string;
    /** The date and time when the content has been modified last */
    lastModified: Date;
    /** The user that has updated the content last */
    lastModifiedBy: string;
    /** The the status of the content */
    status: string;
}
