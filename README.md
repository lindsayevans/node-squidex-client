# Squidex Client

> A JS client for the [Squidex](https://squidex.io/) CMS

[![NPM package](https://img.shields.io/npm/v/@querc/squidex-client)](https://www.npmjs.com/package/@querc/squidex-client)
[![MIT license](https://img.shields.io/npm/l/@querc/squidex-client)](https://opensource.org/licenses/MIT)
[![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/lindsayevans/node-squidex-client)](https://bitbucket.org/lindsayevans/node-squidex-client/addon/pipelines/home)
[![Bitbucket open issues](https://img.shields.io/bitbucket/issues/lindsayevans/node-squidex-client)](https://bitbucket.org/lindsayevans/node-squidex-client/issues)
[![Bitbucket open pull requests](https://img.shields.io/bitbucket/pr/lindsayevans/node-squidex-client)](https://bitbucket.org/lindsayevans/node-squidex-client/pull-requests/)

## Requirements

-   [Node JS](https://nodejs.org/) v12(?)

## Installation

```sh
npm install @querc/squidex-client
```

## Usage

```ts
// Create client
const client = new SquidexClient({
    clientId: 'my-app:default',
    clientSecret: 'SEKRIT',
});

// Get content from the `Posts` schema
const posts = await client.query('Posts');
posts.items.forEach(post => {
    console.log(post.title);
});
```

See [`examples`](https://unpkg.com/@querc/squidex-client/examples/) for more detailed usage.

## TypeScript

Squidex Client is built using TypeScript, so types are installed with the package.

## API Documentation

Check out the [API docs](https://unpkg.com/@querc/squidex-client/docs/index.html).

## Similar projects

-   [Squidex Client Manager](https://www.npmjs.com/package/squidex-client-manager)
-   [squidex-module](https://www.npmjs.com/package/squidex-module) - for Nuxt.js
-   [@awinnen/ngx-squidex](https://www.npmjs.com/package/@awinnen/ngx-squidex) - for Angular

## Contributing

You can raise issues or propose feature requests in the [Bitbucket issue tracker](https://bitbucket.org/lindsayevans/node-squidex-client/issues).

See [`CONTRIBUTING.md`](https://unpkg.com/@querc/squidex-client/CONTRIBUTING.md).
