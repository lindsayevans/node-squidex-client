module.exports = {
    roots: ['<rootDir>/src'],
    testMatch: ['**/__tests__/**/*.+(ts|tsx|js)', '**/?(*.)+(spec|test).+(ts|tsx|js)'],
    // transform: {
    //     '^.+\\.(ts|tsx)?$': 'ts-jest',
    // },
    coverageDirectory: 'test-coverage',
    reporters: [
        'default',
        [
            'jest-junit',
            {
                outputDirectory: './test-reports/',
                outputName: 'jest-junit.xml',
                suiteName: 'Jest tests',
            },
        ],
    ],
};
