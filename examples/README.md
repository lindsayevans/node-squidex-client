# Squidex client examples

1. Copy `.env.example` to `.env` & modify to suit your Squidex app
2. Run `npm run examples` in the top-level directory
