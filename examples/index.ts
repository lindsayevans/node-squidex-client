import * as path from 'path';
import * as dotenv from 'dotenv';

// import { SquidexClient } from '../dist';
import { SquidexClient, SquidexInvariant } from '../src';
// Your code should probably use:
// import { SquidexClient } from 'squidex-client';

// Read environment configuration
dotenv.config({ path: path.resolve(__dirname, '.env') });

class BlogPost {
    title: string;
    slug: string;
    text: string;
}

class BlogPostDTO {
    slug?: SquidexInvariant;
    title: SquidexInvariant;
    text: SquidexInvariant;
}

(async () => {
    // Create a new client
    const client = new SquidexClient({
        // Required:
        clientId: process.env.SQUIDEX_CLIENT_ID,
        clientSecret: process.env.SQUIDEX_CLIENT_SECRET,
        // Optional:
        // url: process.env.SQUIDEX_URL, // Defaults to 'https://cloud.squidex.io'
        // appName: process.env.SQUIDEX_APP_NAME, // Inferred from `clientId`
        languages: ['en', 'es'], // Defaults to all languages
        flatten: true, // Defaults to false
    });

    // Check the configuration
    console.info('Squidex client config:');
    console.log(client.config);

    // // Query the content API for blog posts
    // // 5 latest posts
    // const posts = await client.content('posts').query({ $top: 5 });
    // console.info('5 latest posts:');
    // console.log(posts);

    // // Get a post by slug
    // const slug = 'new-post-1-one-11';
    // const post = await client.content('posts').querySingle<BlogPost>({ $filter: `data/slug/iv eq '${slug}'` });
    // console.info('Get a single post:');
    // console.log(post.data.title);

    // // Get the draft version
    // const draft = await client.content('posts').querySingle<BlogPost>({ $filter: `data/slug/iv eq '${slug}'` }, true);
    // console.info('Get a the draft of a post:');
    // console.log(draft.dataDraft.title);

    // Create a new post
    const post2 = await client.content('posts').create<BlogPostDTO>({
        title: { iv: 'Testing squidex-client' },
        text: { iv: 'This is a test post from the squidex-client module' },
    });
    console.info('Created new post:');
    console.log(` ID: ${post2.id}`);
    console.log(` Status: ${post2.status}`);
    console.log(` Title: ${post2.data.title}`);
    console.log(` Slug: ${post2.data.slug}`);

    // Update the post - you need to send all required fields
    await client.content('posts').update(post2.id, {
        title: { iv: 'Testing squidex-client updates' },
        text: { iv: post2.data.text },
    });

    // Publish the post
    await client.content('posts').updateStatus(post2.id, { status: 'Published' });

    // Patch the post - you can send partial updates
    await client.content('posts').patch(post2.id, {
        title: { iv: 'Testing squidex-client patching' },
    });

    // Discard the post changes
    // For some reason this fails with 'The content has no pending changes' :(
    // await client.content('posts').discard(post2.id);

    // Archive the post
    await client.content('posts').updateStatus(post2.id, { status: 'Archived' });

    // Delete the post
    const deleted = await client.content('posts').delete(post2.id);
    console.info('Deleted post: ', deleted);
})();
