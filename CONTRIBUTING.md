# Contributing to Squidex client

Thanks!

## Things you should check before opening a PR:

1. All unit tests pass
   `npm test`

2. All source files are formatted properly
   `npm run format && npm run lint`

3. Library builds
   `npm run build`

4. Example code runs as expected against build
   `npm run examples`
