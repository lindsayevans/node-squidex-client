## Betas

1. Bump version in package.json
   e.g. 1.2.2 -> 1.2.3-beta.1
2. Run `npm publish --dry-run` to check what will be included
3. Run `npm publish --access public --tag next`

## Full release

1. Run gitflow release
2. Bump version in package.json
   e.g. 1.2.2 -> 1.2.3
3. Run `npm publish --dry-run` to check what will be included
4. Run `npm publish --access public`
